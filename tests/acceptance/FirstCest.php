<?php
class FirstCest 
{
    public function frontpageWorks(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('Home');  
    }
    public function contactUsPageWorks(AcceptanceTester $I)
    {
        $I->amOnPage('/contact-us');
        $I->see('Contact Us');
    }
    
     public function automaionCiPageWorks(AcceptanceTester $I)
     {
         $I->amOnPage('/service/23/codeception');
         $I->see('Codeception');
     }
    
    public function automationTestingPageWorks(AcceptanceTester $I)
    {
        $I->amOnPage('/service/2/automation-testing');
        $I->see('Automation Testing');
    }
}